import "./App.css";
import { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import SignIn from "./components/SignIn/SignIn";
import SignUp from "./components/SignUp/SignUp.js";
import Header from "./components/Header/Header";
import Auth from "./Auth.js";
import Home from "./components/Home/Home.js";
import Write from "./components/Write/Write.js";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AuthObj = new Auth();

function App() {
  const [writePublishBtnClicked, setWritePublishBtnClicked] = useState(false);

  const getAuthenticatedElements = () => {
    return (
      <div className="App">
        <ToastContainer />
        <Router>
          <Header auth={AuthObj} setWritePublishBtnClicked={setWritePublishBtnClicked}/>

          <Routes>
            <Route exact path="/" element={<Home auth={AuthObj} />} />
            <Route exact path="/write" element={<Write auth={AuthObj} writePublishBtnClicked={writePublishBtnClicked} setWritePublishBtnClicked={setWritePublishBtnClicked}/>} />
          </Routes>
        </Router>
      </div>
    );
  };

  const getUnAuthenticatedElements = () => {
    return (
      <div className="App">
        <ToastContainer />
        <Router>
        <Header auth={AuthObj} />
          <>
            <Routes>
              <Route exact path="/signup" element={<SignUp />} />
              <Route exact path="/signin" element={<SignIn auth={AuthObj} />} />
              <Route exact path="/" element={<Home auth={AuthObj} />} />
            </Routes>
          </>
        </Router>
      </div>
    );
  };

  return AuthObj.isAuthenticated()
    ? getAuthenticatedElements()
    : getUnAuthenticatedElements();
}

export default App;
