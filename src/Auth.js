/*
it need to be a class
class methods can be used in every component 
need methods : getToken setToken removeToken isAuthenticated getHeader

*/

export default class Auth {
  getToken() {
    const access = localStorage.getItem("access");
    return access ? access : null;
  }

  setToken(token) {
    try {
      console.log("token", token);
      localStorage.setItem("access", token);
      return token;
    } catch {
      return false;
    }
  }

  removeToken() {
    localStorage.removeItem("access");
  }

  isAuthenticated() {
    return this.getToken() !== null && this.getToken() !== undefined && this.getToken() !== 'undefined'
      ? true
      : false;
  }

  getHeader() {
    const token = this.getToken();
    return token != null ? { Authorization: `Bearer ${token}` } : {};
  }
}
