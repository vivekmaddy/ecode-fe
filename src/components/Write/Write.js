import React, { useEffect, useState } from "react";
import "./Write.css";
import Editor from "./Editor.js";
import { generateToast } from "../common";
import axios from "axios";

export default function Write({
  auth,
  writePublishBtnClicked,
  setWritePublishBtnClicked,
}) {

  // declaration
  const apiURL = process.env.REACT_APP_APIURL;

  const [title, setTitle] = useState("");
  const [story, setStory] = useState("");
  const [typeOfPost, setTypeOfPost] = useState("1");
  const [privacyType, setprivacyType] = useState("public");


  
  //Methods

  const clearForm = () =>{
    setTitle("");
    setStory("");
    setTypeOfPost("");

  }
  const handleSubmitPostRequest = () => {
    try{
      let config = {
        method: "POST",
        url: `${apiURL}/articles/normal_post/`,
        headers: {
          'Authorization' : auth.getHeader()["Authorization"]
        },
        data: {
          title: title,
          content: story,

          created_by: 1,
        },
      };
      axios(config).then(response=>{
        generateToast(response.data.message);
        clearForm();
      }).catch(error=>console.error(error.response))
    }catch(error){
      console.error(error);
    }

  };

  const handleInputTitle = (event) => {
    setTitle(event.target.value);
  };

  const handlePublishSubmit = () => {
    if (title === "") {
      generateToast("Title is missing!");
      return;
    }
    if (typeOfPost === "") {
      generateToast("Type of post missing!");
      return;
    }
    handleSubmitPostRequest();
  };

  const handleTypeOfPostCheck = (event) => {
    if (event.target.checked === true) {
      setTypeOfPost(event.target.value);
    }
  };

  useEffect(() => {
    if (writePublishBtnClicked === true) {
      setWritePublishBtnClicked(false);
      handlePublishSubmit();
    }
  }, [writePublishBtnClicked]);
  return (
    <div className="Write container">
      <div className="row">
        <div className="col-12 mb-4">
          <input
            type="text"
            className="form-control"
            placeholder="Title"
            autoFocus
            value={title}
            onChange={handleInputTitle}
          />
        </div>
        <div className="col-4" style={{display:"grid", alignItems : "center"}}>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="type_of_post"
              id="series_check"
              value="2"
              disabled
            />
            <label className="form-check-label" htmlFor="series_check">
              Series (coming soon)
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="type_of_post"
              id="normal_check"
              value="1"
              onChange={handleTypeOfPostCheck}
              checked
            />
            <label className="form-check-label" htmlFor="normal_check">
              Normal post
            </label>
          </div>
        </div>
        <div className="col-4 text-start">
          <label htmlFor="privacy_select" className="form-label">Choose privacy option :</label>
          <div className="input-group">
            <select className="form-select form-select-sm" aria-label="Default select example" id="privacy_select">
              <option value="1">Public</option>
              <option value="2">Private</option>
              <option value="3">Anonymous</option>
            </select>
          </div>
        </div>
        <div className="col-4 text-start">
            <label htmlFor="formFile" className="form-label">Choose thumbnail :</label>
            <input className="form-control form-control-sm" type="file" id="formFile" />
        </div>
        <div className="col-12 mt-5">
          <Editor setStory={setStory} story={story} />
        </div>
      </div>
    </div>
  );
}
