import React from "react";
import "./PostCard.css";

import BookmarkAddOutlinedIcon from "@mui/icons-material/BookmarkAddOutlined";
import MoreHorizOutlinedIcon from "@mui/icons-material/MoreHorizOutlined";

export default function PostCard({ data }) {
  const content = data.content;

  const getReadTime = (seconds) => {
    const mins = seconds / 60;
    const hours = mins / 60;
    const days = hours / 24;
    if (days >= 1) {
      return `${days} day`;
    } else if (hours >= 1) {
      return `${hours} hour`;
    } else if (mins >= 1) {
      return `${mins} min`;
    } else {
      return `${seconds} seconds`;
    }
  };
  return (
    <div className="PostCard pb-5 pt-4">
      <div className="h mb-3 d-flex align-items-center justify-content-start">
        <div className="head"></div>
        <p className="ms-2">
          {data.created_by.username} <span>· {data.created_at}</span>
        </p>
      </div>
      <div className="b mb-5">
        <h2 className="mb-3">{data.title}</h2>
        <p
          className="short_des"
          dangerouslySetInnerHTML={{ __html: content }}></p>
      </div>
      <div className="f">
        <div className="row">
          <div className="col-8 info">
            <a className="topic" href="/">
              Django
            </a>
            <span className="tm ms-2">{getReadTime(data.read_time)} read</span>
          </div>
          <div className="col-4 d-flex justify-content-end actions">
            <span className="save">
              <BookmarkAddOutlinedIcon />
            </span>
            <span className="ms-4 options">
              <MoreHorizOutlinedIcon />
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
