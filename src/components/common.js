import { toast } from 'react-toastify';


const generateToast = (msg) =>{
    toast(msg)
}

export {generateToast}