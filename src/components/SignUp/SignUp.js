import { useState } from "react";
import "./SignUp.css";
import { useNavigate } from "react-router-dom";

const apiURL = process.env.REACT_APP_APIURL;


export default function SignUp() {
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [showOtptab, setShowOtptab] = useState(false);
  const [signupFormValues, setSignupFormValues] = useState({
    username: "",
    email: "",
    password: "",
    re_password: "",
    otp : ""
  });
  const [passwordMissMatch, setpasswordMissMatch] = useState(false);

  const handleRedirectSignIn = (e) => {
    e.preventDefault();
    navigate("/signin");
  };

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSignupBasicInfoSubmit = (e) => {
    e.preventDefault();
    if (signupFormValues.password !== signupFormValues.re_password){
      setpasswordMissMatch(true)
      return 
    }
    setShowOtptab(!showOtptab);
  };

  const updateSignupFormValues = (event, key) =>{
    const value = event.target.value;
    setSignupFormValues({
      ...signupFormValues, 
      [key]: value.replace(/\s/g, '')
    });
  }

  const getSignupElements = () => {
    return (
      <form method="POST" autoComplete="off" onSubmit={handleSignupBasicInfoSubmit}>
        <div className="mb-3">
          <label htmlFor="username" className="form-label">
            username
          </label>
          <input type="text" className="form-control" id="username" required value={signupFormValues.username} onChange={(event) => updateSignupFormValues(event, 'username')}/>
        </div>
        <div className="mb-3">
          <label htmlFor="email" className="form-label">
            email
          </label>
          <input type="email" className="form-control" id="email" required value={signupFormValues.email} onChange={(event) => updateSignupFormValues(event, 'email')}/>
        </div>
        <div className="mb-3">
          <label htmlFor="password" className="form-label">
            password
          </label>
          <input
            type={showPassword ? "text" : "password"}
            className={passwordMissMatch ? "form-control error" :"form-control"}
            id="password"
            required
            value={signupFormValues.password} onChange={(event) => updateSignupFormValues(event, 'password')}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="repassword" className="form-label"> 
            re-password
          </label>
          <input
            type={showPassword ? "text" : "password"}
            className={passwordMissMatch ? "form-control error" :"form-control"}
            id="repassword"
            required
            value={signupFormValues.re_password} onChange={(event) => updateSignupFormValues(event, 're_password')}
          />
          <div className="invalid-feedback" style={passwordMissMatch ? {display : "block"} : {display : "none"}}>
            both passwords are not matching
          </div>
        </div>
        <div className="mb-3">
          <input
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
            onChange={handleShowPassword}
          />
          <label className="form-check-label ms-2" htmlFor="exampleCheck1">
            show password <span className="mx-2">|</span>
            <a
              href="/signin"
              onClick={handleRedirectSignIn}
              style={{ textDecoration: "none" }}
            >
              already have account?
            </a>
          </label>
        </div>
        <button type="submit" className="btn btn-primary w-100">
          continue
        </button>
      </form>
    );
  };


  const handleFinalFormSubmit = (event) =>{
    event.preventDefault();
    let formValues = Object.values(signupFormValues);
    if (formValues.includes("")){
      navigate("/signup")
      alert("something went wrong")
      return
    }
    
    console.log(signupFormValues)

  }

  const getOTPElements = () => {
    return (
      <form method="POST" autoComplete="off" onSubmit={handleFinalFormSubmit}>
        <div className="mb-3">
          <label htmlFor="otp" className="form-label">
            Please verify using the OTP sent to your email <br/> ({signupFormValues.email})
          </label>
          <input type="text" className="form-control" id="otp" placeholder="otp" required value={signupFormValues.otp} onChange={(event) => updateSignupFormValues(event, 'otp')}/>
        </div>
        <button type="submit" className="btn btn-primary w-100">
          sign up
        </button>
      </form>
    );
  };

  return (
    <div id="SignUp">
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-6">
            <div className="card p-4 d-flex align-items-center justify-content-center">
              <h2 className="mb-4">eCode</h2>

              {!showOtptab ? getSignupElements() : getOTPElements()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
