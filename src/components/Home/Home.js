import React, {useEffect, useState} from "react";
import './Home.css'
import { useNavigate } from "react-router-dom";

import PostCard from '../Post/PostCard.js';

import axios from "axios";

export default function Home({ auth }) {
  const apiURL = process.env.REACT_APP_APIURL;

  const [allPosts, setAllPosts] = useState(null);
  const is_logged = auth.isAuthenticated();
  const navigate = useNavigate();

  // METHODS

  const getAllPosts = () =>{
    try{
      let config = {
        method : "get",
        url : `${apiURL}/articles/all_articles/`,
        headers: {
          'Authorization' : auth.getHeader()["Authorization"]
        },
      };
      axios(config).then(
        response=>{
          if (response.status === 200){
            setAllPosts(response.data.data);
          }
        }
      ).catch(error=>console.error(error));
    }
    catch(error){
      console.error(error);
    }

  }


  useEffect(() => {
    if (!is_logged) {
      navigate("/signin");
    }
  });
  useEffect(() => {
    getAllPosts();
  }, []);
  return (
    <div className="container Home">
      <div className="row justify-content-center" style={{minHeight:"90vh"}}>
        <div className="col-sm-8 l">
          
          {allPosts !== null ? allPosts.map((data,index)=> <PostCard data={data} key={index}/>) : <p>No posts!</p>}
        </div>
        <div className="col-sm-3 r ps-5">
          <div className="r_topics pt-4">
            <h2 className="mb-3">Recommended topics</h2>
            <div className="">
              <a className="badge rounded-pill" href="/">Data Science</a>
              <a className="badge rounded-pill" href="/">Technology</a>
              <a className="badge rounded-pill" href="/">Self Improvement</a>
              <a className="badge rounded-pill" href="/">Writing</a>
              <a className="badge rounded-pill" href="/">Relationships</a>
              <a className="badge rounded-pill" href="/">Machine Learning</a>
              <a className="badge rounded-pill" href="/">Productivity</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
