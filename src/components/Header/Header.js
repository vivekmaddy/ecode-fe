import React from "react";
import "./Header.css";
import { useNavigate } from "react-router-dom";


import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import EditNoteIcon from "@mui/icons-material/EditNote";
import SearchIcon from '@mui/icons-material/Search';

export default function Header({ auth , setWritePublishBtnClicked}) {
  const isLogged = auth.isAuthenticated();
  const navigate = useNavigate();

  const handleNavRedirect = (event, path) =>{
    event.preventDefault();
    navigate(path);
  }
  

  const handleWritePublishBtn = (event) =>{
    event.preventDefault();
    if (setWritePublishBtnClicked !== undefined){
      setWritePublishBtnClicked(true);
    }
  }

  const getLoggedInNav = () => {
    return (
      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <div className="input-group search">
          <span className="input-group-text" id="inputGroup-sizing-default">
          <SearchIcon />
          </span>
          <input
            type="search"
            className="form-control ps-0"
            aria-label="Search"
            aria-describedby="inputGroup-sizing-default"
            placeholder="Search"
            style={{ maxWidth: "15vw" }}
          />
        </div>

        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
          <li className="nav-item" style={{display:"grid", alignItems : "center"}}>
            {
              window.location.pathname !== "/write" ?
              (
                <a className="nav-link" aria-current="page" href="/write" onClick={(event)=> handleNavRedirect(event, "/write")}>
                <EditNoteIcon />
                Write
                </a>
              )
              :
              (
                <button type="button" className="btn btn-sm btn-outline-success rounded-pill px-3" onClick={handleWritePublishBtn}>
                Publish
                </button> 
              )
            }
          </li>
          <li className="nav-item">
            <a className="nav-link mx-2" href="/">
              <NotificationsOutlinedIcon />
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link profileHead" aria-disabled="true" href="/">
              V
            </a>
          </li>
        </ul>
      </div>
    );
  };
  return (
    <div className="Header">
      <nav className="navbar navbar-expand-lg bg-body-tertiary pb-0">
        <div className={isLogged ? "container-fluid main" : "container main"}>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <a className="navbar-brand" href="/">
            eCode
          </a>
          {isLogged && getLoggedInNav()}
        </div>
      </nav>
    </div>
  );
}
