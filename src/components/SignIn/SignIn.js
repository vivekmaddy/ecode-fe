import {useState} from "react";
import "./SignIn.css";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { generateToast } from "../common";


const apiURL = process.env.REACT_APP_APIURL;

export default function SignIn({auth}) {
  const [showPassword, setShowPassword] = useState(false);
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setpassword] = useState("");


  const handleRedirectSignIn = (e) =>{
    e.preventDefault();
    navigate("/signup");
  }

  const handleShowPassword = () =>{
    setShowPassword(!showPassword)
  }


  const handleLoginAPI = () =>{
    try{
      let config = {
        method : 'post',
        url : `${apiURL}/user/login/`,
        data : {
          username : username,
          password : password
        }
      }
      axios(config).then((response)=>{
        if (response.status === 200){
          const access = response.data.data.access
          if (access !== null && access !== "" && access !== undefined){
            auth.setToken(access);
            if (auth.isAuthenticated()){
              window.location.href = "/";
              generateToast("Loggin successful!")
            }
          }
          
        }
      }).catch((error)=>console.error(generateToast(error.response.data.message)))
    }
    catch(error){
      console.error(error)
    }
  }

  const handleLoginSubmit = (event) =>{
    event.preventDefault();
    if(username === "" || password === ""){
      alert("something went wrong")
      return
    }
    handleLoginAPI()


  }

  return (
    <div id="SignIn">
      <div className="container">
        <div className="row align-items-center justify-content-center">
          <div className="col-6">
            <div className="card p-4 d-flex align-items-center justify-content-center">
              <h2 className="mb-4">eCode</h2>
              <form autoComplete="off" onSubmit={handleLoginSubmit}>
                <div className="mb-3">
                  <label htmlFor="username" className="form-label">
                    username or email
                  </label>
                  <input type="text" className="form-control" id="username" required value={username} onChange={(event)=> setUsername(event.target.value)}/>
                </div>
                <div className="mb-3">
                  <label htmlFor="password" className="form-label">
                    password
                  </label>
                  <input
                    type={showPassword ? "text" : "password"}
                    className="form-control"
                    id="password"
                    required
                    value={password} onChange={(event)=> setpassword(event.target.value)}
                  />
                </div>
                <div className="mb-3">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="exampleCheck1"
                    onChange={handleShowPassword}
                  />
                  <label className="form-check-label ms-2" htmlFor="exampleCheck1">
                    show password <span className="mx-2">|</span>
                    <a href="/signup" onClick={handleRedirectSignIn} style={{ textDecoration: "none" }}>
                      create account?
                    </a>
                  </label>
                </div>
                <button type="submit" className="btn btn-primary w-100">
                  sign in
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
